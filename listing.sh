#!/bin/bash
# author : Christophe Airiau, february 2019
# the script make the list of the files of the suite,
# it is able as well to open them with an text editor (here gvim)

# to render this script executable write "chmod a+x listing.sh" into a terminal

Home=~/Documents/Depot_FundAeroSuite
Home=$(pwd)        # this lines can be commented depending of the deposit location in the computer
nbr=$#; 
#echo 'number of arguments ' $nbr

#**************
function aide()
#**************
{
	echo -e "\t *****************************************************"
	echo -e '\n \t\t *****  HELP  ***** \n'
    echo -e "$0 h    \t  : help"
    echo -e "$0 py   \t  : list of python sources"
    echo -e "$0 f90  \t  : list of fortran sources"
    echo -e "$0 m    \t  : list of matlab sources"
    echo -e "$0 all  \t  : list of the files with their details"
    echo -e "$0 a    \t  : list of the files and directories in separated files"
    echo -e "$0 clean \t : clean the home directory"
    echo -e "$0 c     \t : clean the python files *.pyc and the .o files in Fortran/EC"
    echo " if 'e' is the second argument : open the files with gvim"
	echo -e "\t *****************************************************"
	exit
}

#*****************
function present()
#*****************
# file existence
{
file=$1
if [ -e $file ]; then
	   echo 'file ' $file ' found and deleted'
	   rm -f $file
else
	   echo 'file ' $file 'not found and created';
	   touch $file
fi
}

 
#******************************************************************
#  MAIN
#******************************************************************

if [ -z $1 ] || [ $# -lt 1 ]; then
	aide
	exit
fi

cd $Home
case $1 in

    h) aide;;

    c) echo "clean the python files"
        list=$(find ./ -type f | grep pyc)
        echo ${list}
        for file in ${list}
        do
            /bin/rm -f $file
        done
        cd Fortran/EC/src_F90
        make clean
        ;;
    clean) echo "clean this directory"
        rm  -f list_*.txt fortran.txt python.txt matlab.txt tmp.txt
        ;;

    a)echo "list of the files and directories in separated files"
     find ./ -type d > list_directory.txt
     find ./ -type f > list_files.txt
    ;;
    all)echo "list of all files with details"
    file=$Home/list_files.txt
	present $file
    opt=-al
    echo -e "\n#************ \n# LEVEL 0\n#************\n" >> $file
    ls $opt * >> $file
    echo -e "\n#************ \n# LEVEL 1\n#************\n" >> $file
	ls $opt */* >> $file
    echo -e "\n#************ \n# LEVEL 2\n#************\n" >> $file
	ls $opt */*/* >> $file
    echo -e "\n#************ \n# LEVEL 3\n#************\n" >> $file
	ls $opt */*/*/* >> $file
    echo -e "\n#************ \n# LEVEL 4\n#************\n" >> $file
	ls $opt */*/*/*/* >> $file

    ;;
	py)echo "PYTHON"
    file=$Home/python.txt
    ext=.py
    lang=python
    file=$Home/$lang.txt
	present $file

	#********************************
	# PYTHON
	#********************************
    ext=.py
    lang=python
    file=$Home/$lang.txt
	present $file

	dir=Python
	ls $dir/*$ext >> $file
	ls $dir/*/*$ext >> $file
	ls $dir/*/*/*$ext >> $file
	ls $dir/*/*/*/*$ext >> $file

	n_file=$(wc -l $file| cut -d' ' -f1)
	echo " Number of files found in  $lang : " $n_file 

	nline=0
    listfile=''
	exec 3< $file
		while read <&3
		do 
			n=$(wc -l $REPLY| cut -d' ' -f1)
		    echo $REPLY ": n = " $n
            listfile+=" $REPLY"
			let nline+=$n
		done	 
	exec 3>&-
	echo "total $lang = " $nline
    cd $Home
	;;

   f90) echo "FORTRAN"

    #********************************
	# FORTRAN
	#********************************
    ext=.f90
    lang=fortran
    file=$Home/$lang.txt
	present $file

	dir=Fortran
	ls $dir/*$ext >> $file
	ls $dir/*/*$ext >> $file
	ls $dir/*/*/*$ext >> $file
	n_file=$(wc -l $file| cut -d' ' -f1)
	echo " Number of files found in  $lang : " $n_file 

	nline=0
    listfile=''
	exec 3< $file
		while read <&3
		do 
			n=$(wc -l $REPLY| cut -d' ' -f1)
		    echo $REPLY ": n = " $n
            listfile+=" $REPLY"
			let nline+=$n
		done	 
	exec 3>&-
	echo "total $lang = " $nline
    	;;
	m) echo "MATLAB"

    #********************************
	# MATLAB
	#********************************
    ext=.m
    lang=matlab
    file=$Home/$lang.txt
	present $file

	dir=Matlab
	ls $dir/*/*$ext >> $file
	n_file=$(wc -l $file| cut -d' ' -f1)
	echo " Number of files found in  $lang : " $n_file 

	nline=0
    listfile=''
	exec 3< $file
		while read <&3
		do 
			n=$(wc -l $REPLY| cut -d' ' -f1)
		    echo $REPLY ": n = " $n
            listfile+=" $REPLY"
			let nline+=$n
		done	 
	exec 3>&-
	echo "total $lang = " $nline
	;;
esac

if [ $#  -eq 2  ]; then
   gvim $listfile
fi

